<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CustomerTest extends TestCase
{
    #use RefreshDatabase;

    public function test_create_customer(): void
    {
        $data = [
            'name' => 'Gerbes Matos',
            'email' => 'gerbes.matos@gmail.com',
            'birth_date' => '1987-08-27',
            'street' => 'Av prestes maia',
            'number' => '321',
            'complement' => '1303',
            'neighborhood' => 'Centro',
            'postal_code' => '01031001'
        ];

        $response = $this->postJson('/api/customers', $data);

        $response->assertStatus(201);

        $this->assertDatabaseHas('customers', $data);

    }

}
