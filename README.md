<p align="center">
  <a href="mailto:gerbes.matos@gmail.com" target="_blank">
    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTV3JMNlVaF3CdT4wo3Dblm8MbESknkkNidWjg3IYn17M4K6nWNMCHx2Ezj4IPey38JoDE" width="400" alt="Laravel Logo">
  </a>
</p>

# Disys Gerbes Matos

Este projeto foi construído com ênfase no ecosistema do Laravel. Além das funcionalidades padrões, foi adicionado o mecanismos para "meios de pagamento", "checkout" de pedidos e avaliação da experiência.

## Observações

- Utilizei o kickstarter do Sail para "Dockerizar" o projeto, servindo como ambiente de desenvolvimento e homologação.
- Trabalhei com ambiente Linux, com o composer disponível na maquina. desta forma é necessário ter o PHP & Composer para a instalação inicial.
- O e-mail é disparado na criação do pedido. No conceito correto, deveria ser disparado quando o checkout fosse realizado. Os endpoints para cancelamento e checkout são meramente ilustrativos.

## Configuração

Um arquivo `.env` é fornecido junto ao projeto. 
- Você deve modificar a variável `APP_URL`, pois ela é fundamental para o correto funcionamento do sistema de avalições.

- Foi usado o `mailtrap.io` para a validação do e-mail, então tambem é necessário efetuar uma configuração para esta avaliação.

## Como Configurar e Rodar

### 1 - Instalação

```shell
git clone https://gitlab.com/zuroxhub/disys-gerbes-matos  disys-gmatos
cd disys-gmatos
composer i -q
```

### 2 - Ajustes
- Realize os ajustes necessários no arquivo .env.
- Atualize as credenciais do Mailtrap 

### 3 - Homologação

```shell
./vendor/bin/sail up -d
```
```shell
./vendor/bin/sail root-bash
```
#### Database & Seeder
Durante a execução do seed o sistema tentará disparar um e-mail sobre um pedido qualquer, para apresentação.

```shell
chmod 777 storage/ -R &&
php artisan migrate &&
php artisan db:seed
```

## API
Existe uma documentação simples em Markdown

[Clique aqui](api.md) para acessar a documentação da API.

## Open Api
Para melhor entendimento segue uma versão da api para visualização no swagger.

Carregar o [arquivo openapi.yam](openapi.yaml) na plataforma: https://editor.swagger.io/

## Screenshots

#### E-mail com a notificação da confirmação do pedido
É enviado toda vez que um pedido é criado

![mail-confirmation](docs/pastelaria-1.png)

#### Rating para o pedido
Link único com token de acesso para validação do cliente

![mail-confirmation](docs/pastelaria-2.png)

