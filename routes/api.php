<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

use \App\Http\Controllers\CustomerController;
use \App\Http\Controllers\ProductController;
use \App\Http\Controllers\OrdersController;
use \App\Http\Controllers\PaymentMethodsController;

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('customers', CustomerController::class)->except('create','edit');
Route::resource('products', ProductController::class)->except('create','edit');
Route::get('orders',[OrdersController::class,'index'])->name('orders');
Route::post('orders',[OrdersController::class,'store'])->name('order.store');
Route::get('orders/{id}',[OrdersController::class,'show'])->name('order.show');
Route::post('orders/{id}/add-item',[OrdersController::class,'addItem'])->name('order.add-item');
Route::post('orders/{id}/checkout',[OrdersController::class,'orderCheckout'])->name('order.checkout');
Route::post('orders/{id}/cancel',[OrdersController::class,'cancelOrder'])->name('order.cancel');
Route::post('orders/{id}/delete',[OrdersController::class,'destroyOrder'])->name('order.destroy');
Route::get('payment-methods',[PaymentMethodsController::class,'index']);
