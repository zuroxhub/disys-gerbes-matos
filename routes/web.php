<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/public/order-rating/{order_id}',[\App\Http\Controllers\RatingController::class,'index'])->name('rating.index');
Route::post('public/order-rating/{order_id}',[\App\Http\Controllers\RatingController::class,'store'])->name('rating.store');
