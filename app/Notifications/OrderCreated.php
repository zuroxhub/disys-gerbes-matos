<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\HtmlString;

class OrderCreated extends Notification
{
    use Queueable;

    protected $order;

    /**
     * Create a new notification instance.
     */
    public function __construct( Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array<int, string>
     */
    public function via(object $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     */
    public function toMail(object $notifiable): MailMessage
    {

        $items = $this->order->items;
        $table = view('emails.order_table', compact('items'))->render();

        return (new MailMessage)
                    ->subject('Order #' . $this->order->id . ' Confirmation 🎉')
                    ->line('O seu pedido foi confirmado')
                    ->line(new HtmlString($table))
                    ->action('Aproveite e avalie a sua experiência', route('rating.index',[$this->order->id]) . '?token=' . $this->order->rating_token )
                    ->line('Obrigado pela confiança! ');
    }

    /**
     * Get the array representation of the notification.
     *
     * @return array<string, mixed>
     */
    public function toArray(object $notifiable): array
    {
        return [
            //
        ];
    }
}
