<?php

namespace App\Actions;

use App\Models\Order;
use App\Models\PaymentMethod;
use Lorisleiva\Actions\Concerns\AsAction;

class OrderCheckout
{
    use AsAction;

    protected $request;

    /**
     * @throws \Exception
     */
    public function handle($order_id, $request )
    {

        $this->request = $request;

        $this->validatePaymentMethod();

        Order::where('id', $order_id)->update([
            'status' => 'closed',
            'payment_id' => $request->get('payment_id')
        ]);

        return Order::where('id', $order_id)
            ->with('method')
            ->with('customer')
            ->with('items.product')
            ->first();

    }


    protected function validatePaymentMethod()
    {
        try {

            PaymentMethod::where('id', $this->request->get('payment_id') )->firstOrFail();

        }catch ( \Exception $exception )
        {
            throw new \Exception('Payment method is invalid');
        }
    }

}
