<?php

namespace App\Actions;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Class OrderAddItem
 * Adicionar ou incrementar item no pedido
 *
 * @package App\Actions
 */
class OrderAddItem
{
    use AsAction;

    protected $order_id;
    protected $request;
    protected $product_id;
    protected $quantity;
    protected $entity;

    public function handle( $order_id, $request )
    {

        $this->order_id = $order_id;
        $this->request = $request;
        $this->product_id = $this->request->get('product_id');
        $this->quantity = $this->request->get('quantity');

        $entity = OrderItem::where('order_id', $this->order_id)
            ->where('product_id', $this->product_id)
            ->first();

        if($entity){

            // Increment item quantity
            $entity->quantity = $entity->quantity + $this->quantity;
            $entity->save();
        }else{

            // New item
            $entity = OrderItem::create([
                'order_id' => $this->order_id,
                'product_id' => $this->product_id,
                'quantity' => $this->quantity,
                'unit_price' => Product::find($this->product_id)->price,
                'total_price' => Product::find($this->product_id)->price * $this->quantity,
            ]);
        }

        return $entity;

    }

    /**
     * Check is Opened
     *
     * @throws \Exception
     */
    protected function checkIsOpened()
    {
        if( Order::where('id',$this->order_id)->status != "opened")
            throw new \Exception('Order is not opened to add new items');
    }


}
