<?php

namespace App\Actions;

use App\Models\Order;
use App\Models\PaymentMethod;
use Carbon\Carbon;
use Lorisleiva\Actions\Concerns\AsAction;

/**
 * Class OrderCancel
 * @package App\Actions
 */
class OrderCancel
{
    use AsAction;

    public function handle( $order_id,  $request )
    {

        $order = Order::where('id', $order_id)->first();

        if( $order->status != "opened")
            throw new \Exception('Order not opened to cancel');

        $order->status = 'cancelled';
        $order->cancel_date = Carbon::now();

        // Optional
        if($request->has('cancel_reason'))
            $order->cancel_reason = $request->get('cancel_reason');

        $order->save();

        return Order::where('id', $order_id)->first();

    }

}
