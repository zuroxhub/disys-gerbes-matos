<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Order extends Model
{
    use HasFactory;

    public $fillable = [
        'customer_id',
        'payment_id',
        'creation_date',
        'cancel_date',
        'cancel_reason',
        'rating',
        'rating_comment',
        'rating_token',
    ];

    protected $casts = [
        'cancel_date' => 'datetime',
        'creation_date' => 'datetime',
        'checkout_date' => 'datetime'
    ];

    public $timestamps = true;

    protected static function booted()
    {
        static::creating(function ($model) {
            $model->rating_token = Str::random(40);

            while (self::where('rating_token', $model->rating_token)->exists()) {
                $model->rating_token = Str::random(40);
            }
        });
    }

    public function method()
    {
        return $this->hasOne(PaymentMethod::class,'id', 'payment_id');
    }

    public function customer()
    {
        return $this->hasOne(Customer::class,'id', 'customer_id');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

}

