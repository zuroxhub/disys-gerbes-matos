<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory;
    use SoftDeletes;

    public $fillable = [
        'name',
        'description',
        'ean',
        'price',
        'photo'
    ];

    protected $casts = [
        'price'     => 'decimal:2'
    ];

    public $timestamps = true;

}
