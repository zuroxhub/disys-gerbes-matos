<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;
    use Notifiable;

    public $fillable = [
        'name',
        'email',
        'birth_date',
        'street',
        'number',
        'complement',
        'neighborhood',
        'postal_code',
        'registration_date'
    ];

    protected $casts = [
        'birth_date'        => 'date',
        'registration_data' => 'datetime'
    ];

    public $timestamps = true;

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

}
