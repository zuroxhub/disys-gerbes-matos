<?php

namespace App\Http\Requests;

use App\Services\ResponseService;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class ProductUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|min:3',
            'ean' => 'unique:products,ean,' . $this->route('product'),
            'price' => 'required|decimal:2',
            'photo' => 'required|url'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(
            (new ResponseService())->validationError('Validation error', $validator->errors()->all())
        );
    }
}
