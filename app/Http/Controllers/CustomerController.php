<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerStore;
use App\Http\Requests\CustomerUpdate;
use App\Models\Customer;
use App\Services\ResponseService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CustomerController extends Controller
{

    public function index()
    {
        $customers = QueryBuilder::for(Customer::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                'name',
                'email',
            ])
            ->allowedIncludes('orders')
            ->allowedSorts('name', 'email')
            ->get();

        return response()->json($customers);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CustomerStore $request, ResponseService $response)
    {
        try {

            $customer = Customer::create($request->only([
                'name',
                'email',
                'birth_date',
                'street',
                'number',
                'complement',
                'neighborhood',
                'postal_code',
            ]));

            return $response->success( $customer, 'Customer has been created', 201);

        }catch ( \Exception $exception ){

            if($exception instanceof ModelNotFoundException) {
                return $response->notFound();
            }

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id, ResponseService $response)
    {
        try {

            $customer = Customer::where('id', $id)->firstOrFail();
            return $response->success($customer,'Customer details');

        }catch ( \Exception $exception ){

            if($exception instanceof ModelNotFoundException) {
                return $response->notFound();
            }

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CustomerUpdate $request, ResponseService $response, string $id)
    {

        $payload = $request->only([
            'name',
            'email',
            'birth_date',
            'street',
            'number',
            'complement',
            'neighborhood',
            'postal_code',
        ]);

        Customer::where('id', $id)->update($payload);

        return $response->success($payload,'Customer has been updated');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Customer::where('id', $id)->delete();
        return response(204);
    }
}
