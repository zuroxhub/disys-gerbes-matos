<?php

namespace App\Http\Controllers;

use App\Http\Requests\CustomerStore;
use App\Http\Requests\CustomerUpdate;
use App\Http\Requests\ProductStore;
use App\Http\Requests\ProductUpdate;
use App\Models\Customer;
use App\Models\Product;
use App\Services\ResponseService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ProductController extends Controller
{

    public function index()
    {
        $products = QueryBuilder::for(Product::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                'name',
            ])
            ->allowedSorts('name')
            ->get();

        return response()->json($products);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductStore $request, ResponseService $response)
    {
        try {

            $path = $request->file('photo')->store('uploads/temp', 'public');

            $product = Product::create($request->only([
                'name',
                'ean',
                'price',
                'photo'
            ]));

            $newPath = 'uploads/items/' . $product->id . '.jpg';
            Storage::disk('public')->move($path, $newPath);

            $product->photo = $newPath;
            $product->save();

            return $response->success( $product, 'Product has been created', 201);

        }catch ( \Exception $exception ){

            if($exception instanceof QueryException) {
                return $response->error('An error occurred when creating the product', 500, $exception->getMessage());
            }

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id, ResponseService $response)
    {
        try {

            $product = Product::where('id', $id)->firstOrFail();
            return $response->success($product,'Product details');

        }catch ( \Exception $exception ){

            if($exception instanceof ModelNotFoundException) {
                return $response->notFound();
            }

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductUpdate $request, ResponseService $response, string $id)
    {

        try {

            $payload = $request->only([
                'name',
                'ean',
                'price',
                'photo'
            ]);

            Product::where('id', $id)->update($payload);

            return $response->success($payload,'Customer has been updated');

        }catch ( \Exception $exception ){

            if($exception instanceof ModelNotFoundException) {
                return $response->notFound();
            }

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id, ResponseService $response)
    {
        try {

            Product::where('id', $id)->firstOrFail();
            Product::where('id', $id)->delete();
            return response(204);

        }catch ( \Exception $exception ){
            return $response->notFound();
        }
    }
}
