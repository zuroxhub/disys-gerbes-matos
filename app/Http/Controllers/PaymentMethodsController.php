<?php

namespace App\Http\Controllers;

use App\Models\PaymentMethod;
use App\Services\ResponseService;
use Illuminate\Http\Request;

class PaymentMethodsController extends Controller
{
    public function index( ResponseService $response)
    {
        $methods = PaymentMethod::get();
        return $response->success($methods,'Payments methods');

    }
}
