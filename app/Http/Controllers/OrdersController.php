<?php

namespace App\Http\Controllers;

use App\Actions\OrderCancel;
use App\Actions\OrderCheckout;
use \App\Actions\OrderAddItem as ActionOrderAddItem;
use App\Http\Requests\OrderAddItem;
use App\Http\Requests\OrderCreate;
use App\Http\Requests\OrderCheckoutRequest;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Services\ResponseService;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class OrdersController extends Controller
{
    /**
     * Orders Collection
     */
    public function index()
    {
        $customers = QueryBuilder::for(Order::class)
            ->allowedFilters([
                AllowedFilter::exact('id'),
                'method',
                'status',
                'name',
                'email',
            ])
            ->allowedIncludes('customer','items','method','items.product')
            ->allowedSorts('id', 'customer.name')
            ->simplePaginate(10);

        return response()->json($customers);
    }

    /**
     * Create a new Order
     */
    public function store(OrderCreate $request, ResponseService $response)
    {
        $order = Order::create( $request->only([
            'customer_id',
            'payment_id'
        ]));
        foreach ($request->input('items') as $item) {
            OrderItem::create([
                'order_id' => $order->id,
                'product_id' => $item['product_id'],
                'quantity' => $item['quantity'],
                'unit_price' => Product::find($item['product_id'])->price,
                'total_price' => Product::find($item['product_id'])->price * $item['quantity'],
            ]);
        }

        $order->customer->notify( new \App\Notifications\OrderCreated( $order ) );

        return $response->success($order, 'Order has been created');

    }

    /**
     * Order Details
     */
    public function show(string $id, ResponseService $response)
    {
        try {

            $customer = Order::where('id', $id)
                ->with('method')
                ->with('customer')
                ->with('items.product')
                ->firstOrFail();
            return $response->success($customer,'Order details');

        }catch ( \Exception $exception ){

            if($exception instanceof ModelNotFoundException) {
                return $response->notFound();
            }

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }
    }

    /**
     * Add Item
     */
    public function addItem( OrderAddItem $request, ResponseService $response, string $id)
    {

        try {

            ActionOrderAddItem::run( $id, $request );

            return $response->success([],'Item has been updated');

        }catch ( \Exception $exception){

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }

    }

    /**
     * Order Checkout
     */
    public function orderCheckout(OrderCheckoutRequest $request, ResponseService $response, string $id)
    {

        try {

            $order = OrderCheckout::run( $id, $request );
            return $response->success($order,'Order details');

        }catch ( \Exception $exception){

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }

    }

    /**
     * Cancel Order
     */
    public function cancelOrder(Request $request ,ResponseService $response, string $id)
    {

        try {

            $order = OrderCancel::run( $id , $request );
            return $response->success($order, 'Order cancelled');

        }catch ( \Exception $exception){

            return $response->error( $exception->getMessage() , $exception->getCode() ?: 500 );
        }

    }


    public function destroyOrder(string $id, ResponseService $response)
    {
        try {

            Order::where('id', $id)->firstOrFail();
            Order::where('id', $id)->delete();
            return response(204);

        }catch ( \Exception $exception ){
            return $response->notFound();
        }

    }

}
