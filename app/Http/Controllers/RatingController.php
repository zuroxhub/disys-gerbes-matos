<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RatingController extends Controller
{

    public function index( Request $request, $order_id )
    {
        try {

            $rating_token = $request->get('token');

            $order = \App\Models\Order::with('items.product')
                ->where('rating_token',$rating_token)
                ->where('id', $order_id)->firstOrFail();

            return view('order.rating',[
                'order' => $order
            ]);

        }catch ( \Exception $exception ){
            echo 'Invalid Request';
            exit();
        }
    }

    public function store(Request $request, $order_id)
    {

        // somente para apresentar o loading
        sleep(2);

        $request->validate([
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'nullable|string'
        ]);

        $order = \App\Models\Order::where('id', $order_id)->first();

        if($order->rating)
            throw new \Exception('You have already completed this assessment');

        $order->rating = $request->get('rating');
        $order->rating_comment = $request->get('comment');
        $order->save();
        // Retornar uma resposta
        return response()->json(['message' => 'Review sent successfully!']);
    }
}
