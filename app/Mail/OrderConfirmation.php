<?php

namespace App\Mail;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;
use Illuminate\Mail\Mailables\Address;
use Illuminate\Mail\Mailables\Envelope as EnvelopeAlias;

class OrderConfirmation extends Mailable
{
    use Queueable, SerializesModels;

    public $order;

    /**
     * Create a new message instance.
     */
    public function __construct( Order $order )
    {
        $this->order = $order;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): EnvelopeAlias
    {
        return new EnvelopeAlias(
            from: new Address('admin@disys.io,',config("app.name")),
            subject: 'Order #' . $this->order->id . ' Confirmation 🎉'
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            view: 'emails.order-confirmation',
            text: 'emails.order-confirmation-text'
        );
    }

    public function via ($notifiable) {
        return ['mail'];
    }

}
