<?php

namespace App\Services;

class ResponseService {

    public function success($data = [], $message = 'Operation successful', $code = 200) {
        return $this->formatResponse('success', $message, $data, $code);
    }

    public function error($message = 'An error occurred', $code = 400, $data = []) {
        return $this->formatResponse(false, $message, $data, $code);
    }

    public function notFound($message = 'Resource not found', $data = []) {
        return $this->error($message, 404, $data);
    }

    public function unauthorized($message = 'Unauthorized', $data = []) {
        return $this->error($message, 401, $data);
    }

    public function validationError($message = 'Validation error', $errors = []) {
        return $this->formatResponse('error', $message, ['errors' => $errors], 422);
    }

    private function formatResponse($success, $message, $data = [], $code = 200) {
        return response()->json([
            'success' => $success,
            'message' => $message,
            'data' => $data,
        ], $code);
    }
}
