## Pastelaria Premium - API 

Usei o pacote `spatie/laravel-query-builder` na construção das collections da API
- [Spatie/QueryBuilder](https://spatie.be/docs/laravel-query-builder/v5/introduction)

Exemplos de uso:
- /api/orders?include=customer,items 
  - Apresentará as relações ( incluirá automaticamente as relações )
- /api/orders?filter[status]=opened 
  - Filtrando usando a coluna status

### Customers
- `GET` /customers
- `GET` /customers/{id}
- `POST` /customers
- `PUT` /customers/{id}
- `DELETE` /customers/{id}

### Products
- `GET` /products
- `GET` /products/{id}
- `POST` /products
- `PUT` /products/{id}
- `DELETE` /products/{id}

### Orders
- `GET` /orders - Lists all the orders.
- `POST` /orders - Store a new order.
- `GET` /orders/{id} - Show details of order.
- `POST` /orders/{id}/add-item - Add an item to order.
    - `product_id`: (required) The ID of the product. 
    - `quantity`: (required) Numeric value. 
- `POST` /orders/{id}/checkout - Checkout a order.
    - `payment_id`: (required) ID of the payment method.
- `POST` /orders/{id}/cancel - Cancel a order.
    - `cancel_reason`: (optional) Reason for cancellation.
- `POST` /orders/{id}/delete - Delete a order.

### Payment Methods
- `GET` /payment-methods - Lists all payment methods.
