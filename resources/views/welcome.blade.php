<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config("app.name") }}</title>
    <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.x.x/dist/alpine.min.js" defer></script>

    @vite('resources/css/app.css')

</head>
<body class="bg-indigo-200">
<div class="min-h-screen text-center bg-yellow-300 py-6 flex flex-col justify-center sm:py-12">

    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcToSqDSwdDDGWKODJLbAd4Rw92EP9-jBhONug" class="w-[300px] mx-auto rounded-lg opacity-50 shadow-lg">
    <div class="mt-3 text-4xl font-semibold text-yellow-600">{{ config('app.name') }}</div>
    <div class="mt-3 "><a target="_blank" class="text-xs font-bold" href="https://gitlab.com/zuroxhub/disys-gerbes-matos">Git Reposity</a></div>

</div>
</body>
</html>
