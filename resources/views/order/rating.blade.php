@extends('layouts.public')
@section("main")

    <div class="min-h-screen  bg-gray-300 py-6 flex flex-col justify-center sm:py-12">


        <div class="py-3 sm:max-w-xl sm:mx-auto">
            <div class="bg-white min-w-1xl flex flex-col rounded-xl shadow-lg mb-5 p-5">

                <div class="text-3xl">
                    <span>Pedido:</span>
                    <span class="font-bold">#{{ $order->id }}</span>
                </div>

                <div class="text-sm py-4">
                    <div class="grid grid-cols-3">
                        <dl>
                            <dt>Checkin</dt>
                            <dd class="font-bold">{{ $order->creation_date->format('d/m/Y H:i') }}h </dd>
                        </dl>
                        @if($order->checkout_date)
                            <dl>
                                <dt>Checkout</dt>
                                <dd class="font-bold">{{ $order->checkout_date }}</dd>
                            </dl>
                        @endif
                    </div>
                </div>

                <table class="text-xl border-t mt-4">
                    @foreach($order->items as $item)
                        <tr>
                            <td class="text-center whitespace-nowrap">
                                <span class="font-bold">{{ number_format($item->quantity,0) }}</span>
                                <span class="text-sm">un.</span></td>
                            <td class="px-3">
                                <img src="{{ $item->product->photo  }}" class="w-[48px] min-w-[23px] rounded-lg my-2">
                            </td>
                            <td>
                                <div class="font-semibold text-base">{{ $item->product->name }}</div>
                                <div class="text-sm text-gray-400">{{ $item->product->description }}</div>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>

            @if( !$order->rating )
            <div class="bg-white min-w-1xl mt-4 flex flex-col rounded-xl shadow-lg">
                <div class="px-12 py-5">
                    <h2 class="text-gray-800 text-xl text-center font-semibold">Sua avaliação é muito importante para nós!</h2>
                </div>
                <div x-data="{
                        rating: 0,
                        confirmed: false,
                        comment: '',
                        loading: false,
                        submitRating() {
                            this.loading = true;

                            fetch('{{ route('rating.store',['order_id' => $order->id]) }}', {
                                method: 'POST',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                                },
                                body: JSON.stringify({
                                    rating: this.rating,
                                    comment: this.comment
                                })
                            })
                                .then(response => response.json())
                                .then(data => {
                                    console.log(data.message);
                                    // Você pode adicionar alguma ação após o sucesso aqui
                                })
                                .catch((error) => {
                                    console.error('Error:', error);
                                })
                                .finally(() => {
                                    this.loading = false;
                                    this.confirmed = true;
                                });
                        }
                        }"
                     class="bg-gray-200 w-full flex flex-col items-center">

                    <div
                        x-show="loading"
                        class="inline-block m-10 h-8 w-8 animate-spin rounded-full border-4 border-solid border-current border-r-transparent align-[-0.125em] motion-reduce:animate-[spin_1.5s_linear_infinite]"
                        role="status"><span class="!absolute !-m-px !h-px !w-px !overflow-hidden !whitespace-nowrap !border-0 !p-0 ![clip:rect(0,0,0,0)]">Loading...</span>
                    </div>

                    <div class="flex flex-col items-center py-6 space-y-3" x-show="!loading && !confirmed">
                        <span class="text-lg text-gray-800">Como avalia a sua experiência?</span>
                        <div class="flex space-x-3">
                            <svg @click="rating = 1" :class="rating >= 1 ? 'text-yellow-500' : 'text-gray-400'"  class="w-12 h-12 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg @click="rating = 2" :class="rating >= 2 ? 'text-yellow-500' : 'text-gray-400'" class="w-12 h-12 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg @click="rating = 3" :class="rating >= 3 ? 'text-yellow-500' : 'text-gray-400'" class="w-12 h-12 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg @click="rating = 4" :class="rating >= 4 ? 'text-yellow-500' : 'text-gray-400'" class="w-12 h-12 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                            <svg @click="rating = 5" :class="rating >= 5 ? 'text-yellow-500' : 'text-gray-400'" class="w-12 h-12 cursor-pointer" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z" />
                            </svg>
                        </div>
                    </div>
                    <div class="w-3/4 flex flex-col"  x-show="!loading && !confirmed">
                        <textarea x-model="comment" rows="3" class="p-4 text-gray-500 rounded-xl resize-none" placeholder="Você também pode adicionar um comentário"></textarea>
                        <button @click="submitRating" x-bind:disabled="rating === 0 || loading" :class="{ 'opacity-50 cursor-not-allowed': rating === 0 || loading }" class="py-3 my-8 text-lg bg-gradient-to-r from-purple-500 to-indigo-600 rounded-xl text-white uppercase font-bold">Confirmar avaliação</button>
                    </div>

                    <div>
                        <div class="mb-7 mt-5 px-5 text-center font-base text-base text-green-600"  x-show="!loading && confirmed">
                            Obrigado!
                        </div>
                    </div>

                </div>
            </div>
            @else

                <div class="bg-white min-w-1xl mt-4 flex flex-col rounded-xl shadow-lg">
                    <div class="px-12 py-5">
                        <h2 class="text-gray-800 text-xl text-center font-semibold">Sua avaliação é muito importante para nós!</h2>
                    </div>
                    <div  class="bg-gray-200 w-full flex flex-col items-center" >
                    <div class="mb-7 my-6 px-5 text-center font-base text-base text-green-600" >
                        Obrigado!
                    </div>
                    </div>
                </div>
            @endif

            <!--
            <div class="mt-8 text-gray-700">
                UI Crédits <a class="font-bold" href="https://dribbble.com/shots/12052834-Rating-popup">Goga</a>
            </div>
            -->
        </div>
    </div>

@endsection
