<div style="margin-bottom: 20px;">
    <table border="0" cellpadding="10" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Produto</th>
            <th>Quantidade</th>
            <th>Preço Unitário</th>
            <th>Preço Total</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($items as $item)
            <tr>
                <td>{{ $item->product->name }}</td>
                <td>{{ number_format($item->quantity,0) }} <small>un.</small></td>
                <td>R$ {{ number_format($item->unit_price,2,',','.') }}</td>
                <td>R$ {{ number_format($item->total_price,2,',','.') }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
