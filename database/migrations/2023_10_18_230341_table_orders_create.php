<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id()->startingValue(1000);
            $table->unsignedBigInteger('customer_id');
            $table->unsignedBigInteger('payment_id')->nullable();
            $table->enum('status',['opened','closed','cancelled'])->default('opened');
            $table->timestamp('creation_date')->useCurrent();
            $table->timestamp('checkout_date')->nullable();
            $table->timestamp('cancel_date')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->enum('rating',[0,1,2,3,4,5])->nullable();
            $table->text('rating_comment')->nullable();
            $table->string('rating_token');
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('payment_id')->references('id')->on('payment_methods');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
