<?php

namespace Database\Seeders;

use App\Models\PaymentMethod;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PaymentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $payload = [
            'PIX',
            'CRED',
            'DEBIT'
        ];

        foreach ($payload as $item){
            PaymentMethod::firstOrCreate([
                'name' => $item
            ]);
        }
    }
}
