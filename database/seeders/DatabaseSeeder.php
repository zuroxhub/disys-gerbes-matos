<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        $this->call( PaymentsSeeder::class );

        User::factory(1)->create();

        $customers = Customer::factory(2)->create();

        $products = Product::factory(5)->create();

        foreach ($customers as $customer) {
            $orders = Order::factory(rand(1, 5))->create([
                'customer_id' => $customer->id,
            ]);

            foreach ($orders as $order) {

                $randomNumProdutos = rand(1, 3);

                for ($i = 0; $i < $randomNumProdutos; $i++) {
                    OrderItem::factory()->create([
                        'order_id' => $order->id,
                        'product_id' => $products->random()->id,
                    ]);
                }
            }
        }


        try {
            $customer = \App\Models\Customer::first();
            $customer->notify( new \App\Notifications\OrderCreated( $order ) );
        }catch ( \Exception $exception) {
            // Ignorando caso ainda não tenha sido configurado
        }

    }
}
