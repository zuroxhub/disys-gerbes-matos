<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{

    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $pastelNames = ['Pastel de Carne', 'Pastel de Frango', 'Pastel de Queijo', 'Pastel de Camarão', 'Pastel de Chocolate'];

        $pastelImages = range(200,205);

        $name = $this->faker->unique()->randomElement($pastelNames);

        return [
            'name' => $name,
            'description' => "Delicioso $name feito com ingredientes frescos, acompanha maionese verde.",
            'price' => $this->faker->randomFloat(2, 1, 10),
            'ean' => $this->faker->unique()->ean13,
            'photo' => asset('items/'. $pastelImages[ rand(0, count($pastelImages)-1 ) ]).'.jpg'
        ];
    }
}
