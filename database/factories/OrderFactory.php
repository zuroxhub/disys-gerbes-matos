<?php

namespace Database\Factories;

use App\Models\Customer;
use App\Models\Order;
use App\Models\PaymentMethod;
use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Order>
 */
class OrderFactory extends Factory
{

    const TEMPORARY_PAYMENTS = ['PIX','DEBIT'];

    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {

        $paymentIndex = self::TEMPORARY_PAYMENTS[ rand(0, count( self::TEMPORARY_PAYMENTS)-1 )];
        $paymentOption = PaymentMethod::where('name',$paymentIndex)->first();

        return [
            'customer_id' => Customer::factory(),
            'creation_date' => Carbon::now(),
            'payment_id' => $paymentOption
        ];
    }
}
