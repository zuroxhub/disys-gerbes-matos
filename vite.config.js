import { defineConfig } from 'vite';
import laravel from 'laravel-vite-plugin';

export default defineConfig({
    server:{
        host: '192.168.1.87'
    },
    build: {
        outDir: 'public/dist',
    },
    plugins: [
        laravel(['resources/css/app.css', 'resources/js/app.js']),
    ]
});
